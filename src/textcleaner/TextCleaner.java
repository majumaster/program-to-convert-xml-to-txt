/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package textcleaner;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 *
 * @author MajuMaster
 */
public class TextCleaner extends JFrame 
{

    /**
     * @param args the command line arguments
     */
    private JPanel panelSearch = new JPanel();
    private JButton importBtn = new JButton("Import");
    private JButton searchBtn = new JButton("Convert");
    //private JButton testowy= new JButton("testowy");
    private JTextArea textArea = new JTextArea("",7, 10);
    private JScrollPane scrollPane = new JScrollPane(textArea);

    
    private String startMark = "<";
    private String endMark = ">";
    final JFileChooser fileChooser = new  JFileChooser();
    private File [] files;
    
    private int firtPos = 0;
    private int lastPos = 0;  
    
    public TextCleaner()
    { 
        initComponents();
    }

    public void initComponents()
    {
        this.setTitle("Cleaner a files xml");
        this.setBounds(300, 300, 300, 200);
        fileChooser.setMultiSelectionEnabled(true);
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
        "XML and TXT files", "xml", "txt");
        fileChooser.setFileFilter(filter);
        //panelSearch.add(testowy);
        textArea.setEnabled(false);
        panelSearch.add(scrollPane);
        textArea.setText("Dialog Log:\n");
        //testowy.addActionListener((ActionEvent e) -> {
         //   panelSzukania.add(new JLabel("testowy"));
        //    System.out.println("dodany przycisk");
       // });
        panelSearch.add(importBtn);
        importBtn.addActionListener((ActionEvent e) -> {
            int tmp = fileChooser.showOpenDialog(rootPane);
            if (tmp == 0)
            {
                loadFiles(fileChooser.getSelectedFiles());
            }
        });
        panelSearch.add(searchBtn);
        searchBtn.addActionListener((ActionEvent e) -> {
            //szukaj();
            if(files!=null)
            {
                try 
                {
                    clearFiles(files);
                } 
                catch (FileNotFoundException ex) 
                {
                    Logger.getLogger(TextCleaner.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else
            {
                textArea.setText("pusta tablica");
            }
                
        });
        
        this.getContentPane().add(panelSearch, BorderLayout.NORTH);
        this.getContentPane().add(scrollPane, BorderLayout.CENTER);
        this.setDefaultCloseOperation(3);
    }
    public String search(String dataIn) 
    {
        
        firtPos = dataIn.indexOf(startMark, firtPos);
        while(firtPos>=0)
        {
            lastPos = dataIn.indexOf(endMark, firtPos+startMark.length());
            dataIn=dataIn.replace(dataIn.substring(firtPos,lastPos+startMark.length()),"");
            firtPos = dataIn.indexOf(startMark, firtPos);
        }
        return dataIn;
    }
    public void loadFiles(File[] pliki)
    {
        files=pliki;
        for (int i = 0; i < pliki.length; i++)
        {
            textArea.setText(textArea.getText()+ "Loaded "+pliki[i].getName()+"\n");
        }
        textArea.setText(textArea.getText()+"All files loaded!\n");
        
    }
    public void clearFiles(File[]files) throws FileNotFoundException
    {
        for(int i=0;i<files.length;i++)
        {
            try {
                Scanner myReader = new Scanner(files[i]);
                //String[]name=files[i].getName().split(".txt",".xml");
                String name=files[i].getName().substring(0, files[i].getName().length()-4);
                File newFile=new File(name+".txt");
                if(!newFile.createNewFile())
                {
                    int n = JOptionPane.showConfirmDialog(rootPane,"Would you like to overwrite a "+newFile.getName()+" file?","Overwrite Files",JOptionPane.YES_NO_OPTION);
                    if(n==1)
                        continue;
                }
                FileWriter myWriter = new FileWriter(newFile);
                //tworzy nowy plik
                while (myReader.hasNextLine()) {
                    String data = myReader.nextLine();
                    //wpisuje czyste zdanie do nowego pliku
                    myWriter.write(search(data)+"\n");
                    //System.out.println();
                }
                myReader.close();
                myWriter.close();
                textArea.setText(textArea.getText()+"Saved "+newFile.getName()+"\n");

            } 
            catch (FileNotFoundException e) 
            {
                System.out.println("An error occurred.");
            } 
            catch (IOException ex) {
                Logger.getLogger(TextCleaner.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        textArea.setText(textArea.getText()+"All files saved!\n");
    }
    
     

    public static void main(String[] args) 
    {
        // TODO code application logic here
        new TextCleaner().setVisible(true);
    }
}
